import { mapActions, mapState } from 'vuex'
import store from '@/store'

export default {
    name: 'employees',
    data () {
        return {
            showModal: false,
            showModalEdit: false,
            showModalInfo: false,
            name: '',
            birth_date: '',
            bank_account: '',
            bank: '',
            id_number: '',
            phone_number: '',
            rap_number: '',
            rtn_number: '',
            contract_type: '',
            job: '',
            turn: '',
            payment_cycle: '',
            employee: '',
            feedback: ''
        }
    },
    methods: {
        ...mapActions(['getPaymentCycles', 'getEmployees']),
        newEmployee() {
            // validate that no field is empty
            if (this.name  && this.birth_date  && this.bank_account && this.bank && this.id_number && this.phone_number && this.rap_number && this.rtn_number && this.contract_type && this.job && this.turn && this.payment_cycle_id) {
                store.dispatch('newEmployee', {
                    name: this.name,
                    birth_date: this.birth_date,
                    bank_account: this.bank_account,
                    bank: this.bank,
                    id_number: this.id_number,
                    phone_number: this.phone_number,
                    rap_number: this.rap_number,
                    rtn_number: this.rtn_number,
                    contract_type: this.contract_type,
                    job: this.job,
                    turn: this.turn,
                    payment_cycle_id: this.payment_cycle
                })
            }
            else{
                this.feedback = '* Por favor llenar todos los campos';
            }
        },
        showInformation(employee){
            this.employee = employee;
            this.showModalInfo = true;
        },
        editEmployee(){
            store.dispatch('editEmployee', {id})
        },
        deleteEmployee(id){
            store.dispatch('deleteEmployee', {id})
        }
    },
    created(){
        this.getPaymentCycles(),
        this.getEmployees()
    },
    computed: {
        ...mapState(['payment_cycles', 'employees'])
    },
    mounted(){

    }
}