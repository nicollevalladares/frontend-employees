import Vue from 'vue'
import Router from 'vue-router'
import employees from '@/components/employees'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'employees',
      component: employees
    }
  ]
})
