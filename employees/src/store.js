import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import server from '../config/server'

Vue.use(Vuex)


export default new Vuex.Store({
    state: {
        payment_cycles: [],
        employees: []
    },
    mutations: {
        setPaymentCycles(state, payment_cycles){
            state.payment_cycles = payment_cycles;
        },
        setEmployees(state, employees){
            state.employees = employees;
        }
    },
    actions: {
        getPaymentCycles({commit}){
            axios.get(`${server.mainServe}/payment_cycles`)
            .then(response => {
                const payment_cycles = [];

                response.data.forEach(payment_cycle => {
                    var data = {
                        value: payment_cycle.id,
                        text: payment_cycle.name
                    }
                    payment_cycles.push(data);
                });

                commit('setPaymentCycles', payment_cycles)
            })
        },
        getEmployees({commit}){
            axios.get(`${server.mainServe}/employees`)
            .then(response => {
                const employees = response.data;

                commit('setEmployees', employees)
            })
        },
        newEmployee({commit}, payload){
            axios.post(`${server.mainServe}/employees`, {
                name: payload.name,
                birth_date: payload.birth_date,
                bank_account: payload.bank_account,
                bank: payload.bank,
                id_number: payload.id_number,
                phone_number: payload.phone_number,
                rap_number: payload.rap_number,
                rtn_number: payload.rtn_number,
                contract_type: payload.contract_type,
                job: payload.job,
                turn: payload.turn,
                payment_cycle_id: payload.payment_cycle_id
            })
            .then(response => {
                window.location.reload()
            })
        },
        editEmployee(){

        },
        deleteEmployee({commit}, payload){
            axios.delete(`${server.mainServe}/employees/${payload.id}`)
            .then(response => {
                window.location.reload()
            })
        }
    },
    getters: {
            
    }
});